import requests
from src.settings.config import BOT_URL, SERVICES


class Checker:
    def __init__(self):
        self.services: dict = SERVICES
        self.bot_url: str = BOT_URL

    def send_info_to_bot(self, service, options: dict, message: str):
        i = {"service": service, "url": options["url"], "reason": message}
        requests.post(BOT_URL, json=i)

    def send_info_to_bot(self, service, options: dict, message: str):
        i = {"service": service, "url": options["url"], "reason": message}
        requests.post(BOT_URL, json=i)

    def check_services(self):
        for service, options in self.services.items():
            try:
                r = requests.get(options["url"], timeout=int(options["timeout"]))
            except requests.exceptions.Timeout:
                self.send_info_to_bot(service, options, "timeout error")
            match r.status_code:
                case 200:
                    continue
                case _:
                    self.send_info_to_bot(service, options, f"response {r.status_code}")
                    continue
