import json
from dotenv import dotenv_values

config = dotenv_values(".env")

BOT_URL = config["BOT_URL"]
BOT_TOKEN = config["BOT_TOKEN"]
with open("src/settings/services.json", "r") as f:
    SERVICES = json.loads(f.read())
