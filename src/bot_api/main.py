import telebot
from src.bot_api.models import Info
from src.settings.config import BOT_TOKEN, SERVICES
from fastapi import FastAPI

app = FastAPI()
bot = telebot.TeleBot(BOT_TOKEN)


@app.post("/")
async def send_info_to_chat(info: Info):
    alert_chats = SERVICES[info.service]["alert_addressees"]
    for _name, chat_id in alert_chats.items():
        bot.send_message(chat_id=chat_id, text=f"{info.url}\n{info.reason}")
