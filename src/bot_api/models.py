from pydantic import BaseModel


class Info(BaseModel):
    service: str
    url: str
    reason: str
